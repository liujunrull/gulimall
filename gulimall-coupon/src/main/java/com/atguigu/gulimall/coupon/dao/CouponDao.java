package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author liujunru
 * @email 978182116@qq.com
 * @date 2022-01-10 22:12:12
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
