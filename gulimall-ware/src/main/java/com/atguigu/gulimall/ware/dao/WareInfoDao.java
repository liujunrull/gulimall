package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author liujunru
 * @email 978182116@qq.com
 * @date 2022-01-10 22:38:53
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
