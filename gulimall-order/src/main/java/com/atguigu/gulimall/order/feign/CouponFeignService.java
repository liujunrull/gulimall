package com.atguigu.gulimall.order.feign;

import com.atguigu.commom.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * member远程调用coupon
 */
//表示要远程调用gulimall-coupon服务
@FeignClient("coupon")
public interface CouponFeignService {
    //远程调用的优惠券查询
    @RequestMapping("/coupon/coupon/member/list")
    public R memeberCoupons();
}
