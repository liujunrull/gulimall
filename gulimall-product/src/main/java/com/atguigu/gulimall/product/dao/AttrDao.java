package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author liujunru
 * @email 978182116@qq.com
 * @date 2022-01-10 21:00:44
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
